## Prevention of Sexual Harassment

   # sexual Harassment Causes
  
     * Making sexual jokes.
     * Repeated compliments of an employee's appearance.
     * Commenting on the attractiveness of others in front of an employee.
     * Discussing someone's sex life in front of a colleague.
     * Circulating nude photos of women in bikinis or shirtless men in the workplace.
     * Interrogating an employee about their sex life.
     * Leaving unwanted gifts of a sexual or romantic nature.
     * Spreading sexual rumours about a colleague.
     * Repeating hugs or other types of unwanted touching.
   # Action should Be Taken Against Sexual Harassment
     
     * Speak out against the harasser:This is often the simplest andmost direct way to deal with sexual harassment.By getting to the root cause of it and addresing the provoker,We have make our message loud and clear that the behavior in question is not acceptable.
      
     * Report the harassment to the appropriate people: Taking it to the next level, we have to choose to take the harassment incident(s) to a supervisor, human resources department or internal social case worker. Make sure to follow exactly whatever company policies or procedures are in place so that our case will be treated with the appropriate timeliness and seriousness. Our company should be able to handle the sexual harassment charge from then on, either by dismissing or counseling the harasser, separating the harasser and the victim, or another method.

    * File a charge with our state human or civil rights establishment: This next step in dealing with sexual harassment is a major one. I will contact the governing agency in my state which deals with issues of workplace discrimination and harassment, which is usually the Equal Employment Opportunity Commission. This enforcement entity will typically strive to resolve the claim with our employer. If this fails, they may advise that you bring a lawsuit against the company.

    * Take legal action: Lastly, the most dramatic step we can take in dealing with sexual harassment in the workplace is filing a lawsuit against our employer. While this type of litigation may seem drastic, it is sometimes the only procedure that gets results, as long as the three above steps failed to resolve the problem. Victims of sexual harassment often file lawsuits against an employer for physical or emotional suffering, and they may be able to get their jobs back, receive lost wages and acquire damage costs for the events that occurred.

 # Different Scenarios Enacted By Actors

   * Artistic Freedom:Sexual nature posted either on a nulletin board or on a wall.
   * You Gotta Keep TryIn:If a person ask for something for over and over it leads to work environment sexual harassment.
   * The Joke's On You:Jokes ,cartoons the ever popular email jokes on your computer that are sexual in nature.
   * The Legend:If the other person is unwelcomed if it were continued it could constitute to hostile work environment sexual harassment.
   * Odd Man Out:Whenever you manage people it's important to trat them equally performance should be the only thing.

 #  Behave Ourself
   
   * Be mindful of your noise level at all times:Controlling our noise level is the biggest part of behaving. If you are having trouble maintaining an appropriate volume, stop and take a deep breath when you feel yourself getting louder. Collect your thoughts and convey what you want to say in a respectful and reserved manner. Pay attention to your noise level and you can better control it.
   * Practice self-control in all aspects of your life by setting goals and sticking to them:Being goal-oriented will teach you the art of self-denial. If you get the desire for a soda or to slack off and play video games, deny yourself. Start with small goals like denying yourself ice cream on weekdays. Move up to harder goals like making the starting basketball team. Stick with your goals and soon enough you'll be in total control of your thoughts and actions.
   * Pay careful attention to rules and social norms, and restrain yourself when you get the urge to break them.Calming techniques like deep breathing usually work best for self-control, but find what works best for you. Maybe if you're about to break a rule, you can snap your fingers or pinch yourself. Whatever it is, have a method to stop yourself when you get the urge to break a rule.
   * Be careful with your language:Suit your words to the situation, and choose those words carefully. In most situations, cursing and loud bickering is inappropriate. Avoid pointless arguments and critical statements. Generally speaking, if you don't have something nice to say, don't say it. If you find yourself starting to say something mean or inappropriate, stop yourself before it's too late. If you can't stop yourself, be sure to apologize after
