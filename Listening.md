# Listening and Active Communication

## 1.Active Listening

   - Use your body language to show you are listening to them.
   - Make eye contact with the speaker.
   - Focus on the speaker; don’t get distracted by the thoughts racing in your mind.
   - Don’t interrupt the person speaking, let them finish first and then respond.
   - Show them your interest in the speaker’s topic by encouraging them to speak more. For example, say “That sounds interesting” or “Tell me more”.
   - Paraphrase and summarize. This also ensures you and the speaker are on the same page.

## 2. Reflective Listening - Fisher's Model
 
   - Actively listening to the speaker. Remove all the distractions to pay full attention.
   - Listen more, talk less.
   - Avoid pretending like you have listened to them or show fake concerns.
   - Focus on the speaker’s mood as well. This can be studied by their facial expressions, tone of their voice and other non-verbal cues.
   - Don’t give long or complex responses as this confuses the speaker. Stick to short and simple responses.
   - Summarize whatever the speaker has spoken. This will show that you are listening to them.
   - Empathize the speaker's point of view, encouraging the speaker to speak freely without feeling judged.

## 3. Obstacles in Listening Process

   - There are two types of obstacles everyone faces called external and internal obstacles.
   - The external obstacle are like:
     - Any external noise.
     - Distraction from sight.
   - We can manage extranal obstacle compared to internal obstacle.
   - Internal obstacle are like
      -  Anxiety doesn’t allow one to focus on what is being said to them and is found often competing with their own worries.
      -  Mental laziness to process complex or detailed information the speaker is giving.

## 4.How to improve your listening skilss?
 
   - Listen more and speak less by avoiding interrupting the speaker. Best ways is to close your mouth as open mouth gives signal to the speaker that you want to interrupt, or simply write down on notes if you don’t want to lose any ideas you were about to say.
   - Show them that you are interested in listening to them more by using positive body language and saying phrases like “Yes”, “Really?” or “That sounds interesting”.
   -  Avoid jumping into conclusions or judging someone when they are speaking by agreeing and valueing their opinions or ideas.
   - Focus fully on the speaker, avoid making the speaker feel you aren’t listening by checking down your phone, daydream, looking here or there, or other negative signs.
   - After they finished their we have to respond.

##  5.Situations that causes to switch into Passive communication style
 
   - Some situations are addressed below where I turn to passive communication:
   - When other people's happiness is more important than mine.
   - When I value others and don’t value myself.
   - When I feel my opinions or feelings don’t even matter to the people.

## 6.Situations that causes to switch into Aggressive communication style

   - Some of the situations that causes to switch into Aggresive communication style
     - Without listening to other what they are saying we simply talk
     - When i make any mistake but find it that i made a miskake ,but refuse to sorry to other people.
     - when other don't listen me when I am speaking.  

## 7. Situations that causes to switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles
  
   - When someone shows their face only when they need something from me.
   - When I am fed being silent for others at the expense of my peace.
   - When someone takes away my rights or doesn’t listen to me.
   - When someone said something hurtful.

## 8.How can you make your communication assertive?
  
   - Learn to recognize and name what you need.
   - Start with low-stakes situations.
   - Be aware of your body language.
   - Speak up about situations that are problematic for us as early as possible.
   - Learn to recognize and name your feelings.

 
    