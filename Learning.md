## Learning Process

 # Feynman Technique
   
   Richard Feynman was a Nobel Prize winning physicist who made significant contributions in areas such as quantum mechanics and particle physics. He also pioneered quantum computing, introducing the concept of nanotechnology. He was a renowned lecturer who taught at Cornell and Caltech.
    
   Despite all of his accomplishments, Feynman thought of himself as “an ordinary person who studied hard”. He believed that anyone was capable of learning with enough effort, even complex subjects like quantum mechanics and electromagnetic fields.
    
   The Feynman Technique is a learning concept we can use to understand just about anything.

   The Feynman Technique is a four-step process for understanding any topic. This technique rejects automated recall in favor of true comprehension gained through selection, research, writing, explaining, and refining. 

  # Different Ways To Implement Feyman Technique
    
     * Choose a Concept to Learn:We have to choose one topic. By writing a topic down on a blank page, you acknowledge you’re starting from scratch or at least filling in some blanks. In doing so, take the initial step in the process.

     * Explain it to yourself or teach it to someone else:When you have to truly explain something, whether through writing or aloud, you encounter the holes in your reasoning and the white spaces in your knowledge. Think of writing and teaching as a process to obtain understanding, not something you do once you already understand.

     * Return to the source material if you get stuck:Using sources to polish our own explanations and models is an active process. When we learn passively, committing details to memory is more challenging. When we’re actively part of creating our own summaries and reasoning, drawing intentionally from original information to fill our blind spots, we can more readily commit knowledge to our long-term memory.

     * Simplify your explanations and create your own analogies:It’s easy enough to commit terms to memory, and repeat them back when prompted. But memorization is not understanding. When we can’t rely on big words that make us sound smart, we have to distill what we truly know to the most basic form. This is where true understanding takes place.

  # Learning How to Learn
   
     * Use Recall:After read a page, look away and recall the main ideas. Highlight very little, and never highlight anything you haven’t put in your mind first by recalling.
     * Chunk Problems:Chunking is understanding and practicing with a problem solution so that it can all come to mind in a flash.
     * Space Your Repetition:Spread out your learning in any subject a little every day, just like an athlete.Our can handle only a limited amount of exercise on one subject at a time.
     * Alternate different problem‐solving techniques during your practice. Never practice too long at any one session using only one problem‐solving technique—after a while, you are just mimicking what you did on the previous problem.
     * Focus. Turn off all interrupting beeps and alarms on your phone and computer, and then turn on a timer for twenty‐five minutes. Focus intently for those twenty‐five minutes and try to work as diligently as you can.
     * Eat your frogs first. Do the hardest thing earliest in the day, when you are fresh.
     * Make a mental contrast. Imagine where you’ve come from and contrast that with the dream of where your studies will take you.

  # Steps to Improve Learning Process
    
     * Always we have to recall what we are using we have to connect to present world and implement physically.
     * We have do many practice problems to tearn the concept clearly.
     * We have to teach to other persons everytime what we have learned.

  # How to Learn Anything
    
     * Deconstruct The Skill:Deconstructing a skill into smaller bundles allows you to focus on what helps you arrive at your desired outcome. Take what you need, and leave the others for another day.
     T he learning curve is set up in such a way that beginners can derive gains quickly at the start
     * Learn Enough To Self Correct:The act of acquiring information becomes more important than the actual act of mastery.
     * Remove Practice Barriers:Focus on improving our environment and make it conducive for practice. Remove the distractions that prevent us from sitting down and practice.
     * Spend The Actual 20 Hours:There is a frustration period when we first start out because we are grossly incompetent. We feel stupid.But we need to work on that.

  #  Steps to Approaching a New Topic
     
     * First of all i will see all the resources that are available to me.
     * Then i will implement to the practical field.
     * For example if i don't know about closure concept in the javascript, then i will read the concept in google or from youtube.Then i will implement tht problems in vscode.
     * If i had enough knowledge then i practice more problems to gain deep knowledge about closure.Afterwards i only create new problems.

    

       











    

     
