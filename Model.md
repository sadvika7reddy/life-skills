# Open Systems Interconnection(OSI)

# Defination
The OSI Model is a conceptual model that characterises and standardises the communication functions of a telecommunication.or computing system without regard to its underlying internal structure and technology. Its goal is the interoperability of diverse communication systems with standard protocols
# The OSI Model
OSI stands for “Open Systems Interconnection”. The exact definition of the OSI Model has been already given above. In simpler words, to be precise, the OSI model is a tool used by IT professionals to actually model or trace the actual flow of how data transfers in networks.
# Why OSI Model
The purpose of the OSI reference model is to guide vendors and developers so the digital communication products and software programs they create will interoperate,and to facilitate clear comparison among communications tools.
# Seven Layers of OSI Model
OSI Model is composed of seven layers with the application layer.
1. Physical Layer
   * This is the layer on which the real transmission of data bits takes place.
   * All the physical stuff that connects the computer together.
2. Data Link Layer
   * This Layer is responsible for organising bits into frames.
   * THis is the layer on which the Switches operate on.
3. Network Layer
    * The main job of this layer is to move packets from source to destination and provide inter-networking.
    * This is the layer that the routers operate on.
4.  Transport Layer
    * This layer has a very important job
    *  This layer will decide how much data you can transfer and receive at a given point of time.
5. Session Layer
    * This layer has the job of maintaining proper communication
    *  This layer terminate session s between two computers.
6. Presentation Layer
    * In this layer operating system operates with the data.
    * Basically User interacts with Application layer, which sends the data down to Presentation layer.
7. Application Layer
    * This is the topmost layer in the seven OSI Layers.
    * This is the layer that the end-user (can be a computer programmer, or a regular PC user) is actually interacting with.
    * This layer allows access to network resources.
# Conclusion
This was a short and to the point article explaining the OSI Model, to help out to understand the model.All the seven layers of OSI Model explained.
# References
1. https://www.youtube.com/watch?v=-6Uoku-M6oY
2. https://www.youtube.com/watch?v=vv4y_uOneC0